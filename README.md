# Readme

The VT Ansible Roles project is no longer maintained, and all repositories have been archived. Feel free to use them as an example, but there is no guarantee the they will work or be secure.